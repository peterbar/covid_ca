[![eRum2020::CovidR](https://badgen.net/https/runkit.io/erum2020-covidr/badge/branches/master/petr-baranovskiy-covid-ca-data-explorer?cache=300)](https://milano-r.github.io/erum2020-covidr-contest/petr-baranovskiy-covid-ca-data-explorer.html)

### What Some Indicators Mean

<u>Total cases</u> are all cases since the start of the epidemic, i.e. cumulative cases. For the number of people currently ill, see "Active cases".

<u>Cases per 100,000</u> indicator shows the overall [prevalence](https://www.britannica.com/science/prevalence) per 100,000 population since the pandemic started. For the share of people who are currently ill, see "Active cases per 100,000".

<u>Total tested / Total tests</u>, <u>New tested / New tests</u>, <u>Tested per 1,000 / Tests per 1,000</u>: You will see a **very** large jump in the number of tests on February 1st, 2021. As of that date, the Government of Canada started using a different method of reporting the number of COVID tests in its [official COVID19 dataset](https://health-infobase.canada.ca/src/data/covidLive/covid19.csv). Previously, test counts were being reported as a *numtested* variable, and starting February 1st, 2021, COVID tests numbers aree being reported as a *numtests* variable with much higher counts, while the *numtested* variable now contains only missing values. This likely means that previously the Government of Canada was reporting the number of *people* tested, and now it reports the number of *tests* performed. I decided to join these two variables into the same time series for the sake of continuity.

<u>Case fatality rate</u> shows a percent of those who have died among the *diagnosed* cases. [Case fatality rate](https://www.britannica.com/science/case-fatality-rate) should not be confused with [mortality rate](https://en.wikipedia.org/wiki/Mortality_rate). For mortality rate, see "Mortality per 100,000".

### Data

The [data](https://health-infobase.canada.ca/src/data/covidLive/covid19.csv) is downloaded from the Government of Canada [official COVID-19 page](https://www.canada.ca/en/public-health/services/diseases/2019-novel-coronavirus-infection.html) at 6-hour intervals to minimize the load on the repository. Thus there may be a delay of up to 6 hours from the time [canada.ca](https://www.canada.ca/en.html) update their data to the moment it gets updated on my server.

To calculate “Cases per 100,000”, “Active cases per 100,000”, “New cases per 100,000”, “Mortality per 100,000”, and “Tests done per 1,000”, I use Statistics Canada population estimates for the corresponding quarter. If the estimates for the latest quarter are not yet available, estimates for the previous quarter are used. Source: Statistics Canada Data table [17-10-0009](https://www150.statcan.gc.ca/t1/tbl1/en/tv.action?pid=1710000901).

Geospatial data used to render the map was retrieved [from Statistics Canada](http://www12.statcan.gc.ca/census-recensement/2011/geo/bound-limit/files-fichiers/2016/lpr_000b16a_e.zip). The polygons were then simplified to ensure quick rendering of the map.

### Data Issues

The data prior to March 21, 2020 was truncated due to being, for most indicators, highly incomplete or missing entirely, which resulted in various issues when computing and visualizing epidemiological indicators.

The dataset at some point contained some few negative numbers, which don't make sense in this context. These are/were almost certainly data entry errors on the part of the government, so I fixed this issue by converting all numbers in the dataset to their absolute values. There are some other obvious data entry errors in the dataset. Unless indicated otherwise here, data entry errors are being converted to missing values (you will see these as gaps in time series).

Re the huge spike in the number of tests on February 1st, 2021, see "What some indicators mean" above.

The map color palette may look different depending on the indicator and/or the date you have selected. This is due to some of the data being highly skewed, which causes <span style="font-family:Ubuntu Mono;">leaflet::colorQuantile</span> to [fail](https://github.com/rstudio/leaflet/issues/94). I had to program around this issue by creating a function that switches to <span style="font-family:Ubuntu Mono;">leaflet::colorNumeric</span> in such cases. <span style="font-family:Ubuntu Mono;">colorNumeric</span> doesn't use quantiles to break down the data, so the resulting color scheme doesn't look as good.

### About the Author

My name is [Petr Baranovskiy](https://dataenthusiast.ca/about/), I am an R language enthusiast, and I specialize in economic policy analysis, economic and statistical modeling, energy policy, and the use of geospatial data for policy analysis. If you liked this app, please visit my blog at [dataenthusiast.ca](https://dataenthusiast.ca/).

### Source Code

You can find the app's source code [here](https://gitlab.com/peterbar/covid_ca). And [here](https://gitlab.com/peterbar/covid_ca_base) is the code used to retrieve and pre-process geospatial data for provincial and territorial boundaries.

### License and Disclaimer

Click [here](https://gitlab.com/peterbar/covid_ca/-/raw/master/LICENSE.txt?inline=false) to read the license.

<span style="font-family:Lato; font-size: 12px;">THE USE OF THE GOVERNMENT OF CANADA AND/OR STATISTICS CANADA DATA IN THIS SOFTWARE DOES NOT CONSTITUTE AN ENDORSEMENT BY THE GOVERNMENT OF CANADA AND/OR STATISTICS CANADA OF THIS SOFTWARE.</span>

<span style="font-family:Lato; font-size: 12px;">THE APPLICATION IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR OR COPYRIGHT HOLDER BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OR CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE APPLICATION OR THE USE OR OTHER DEALINGS IN THE APPLICATION.</span>
