Click region names in the plot legend to add/drop regions.

Pointing mouse cursor at the lines in the plot will bring up pop-ups with more information. It will also open a pop-up menu in the top right corner: ![](https://dataenthusiast.ca/wp-content/uploads/2020/12/plotly_menu.png)

Use it to adjust the plot to your liking. Particularly, check out these two options:
  - ![](https://dataenthusiast.ca/wp-content/uploads/2020/12/plotly_hover_show_closest.png) show closest data on hover, and
  - ![](https://dataenthusiast.ca/wp-content/uploads/2020/12/plotly_hover_compare_data.png) compare data on hover.

Click and drag mouse to select parts of the plot for closer inspection. Double-click to return back to the entire plot.
