[![eRum2020::CovidR](https://badgen.net/https/runkit.io/erum2020-covidr/badge/branches/master/petr-baranovskiy-covid-ca-data-explorer?cache=300)](https://milano-r.github.io/erum2020-covidr-contest/petr-baranovskiy-covid-ca-data-explorer.html)

COVID-19 Canada Data Explorer app by Petr Baranovskiy @ [dataenthusiast.ca](https://dataenthusiast.ca/)

The app is available at: https://dataenthusiast.ca/apps/covid_ca/

For more details, please see the in-app 'About' page and my [blog post about the app](https://dataenthusiast.ca/2020/covid-19-canada-data-explorer/). 